module pt.isec.gps2324.gps_g14 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    requires com.almasb.fxgl.all;
    requires eu.hansolo.tilesfx;

    opens pt.isec.gps2324.gps_g14 to javafx.fxml;
    exports pt.isec.gps2324.gps_g14;
}