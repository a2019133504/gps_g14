# Report

To use the different operations provided by the app, you first need to run *ConsoleApp*, after that the user is presented with a menu that details the available commands and modules to choose.

The general commands that are provided are ‘help’ and ‘exit’ that show the menu again and exit the app/module respectively.

When selecting a module the user is presented with the menu of that module that details the available operations and commands.

## Modules

In the calculator module the user is asked to input a math operation like ‘1+1.2’ and the module will respond with the result of said operation.

In the fibonacci/factorial module the user is asked to choose the operation he wants to use.
For the fibonacci, he’s asked to type the number of iterations he wants to see in the sequence and the module will respond with all the numbers of it.

For the factorial operation, the user is asked to type the number with which he wants to calculate the factorial and the module will respond with the full operation and the result of it.

<aside>
💡 hexa/bin module missing
</aside>

In the OperationsDates module, the user is prompted to input a date and has two options: either add/subtract a specified number of days from the date ('10/10/2023' '7' or '-7'), or input another date and calculate the difference between the dates ('10/10/2023' and then '10/12/2023'). The module will respond with the result of the date operation.

In the Volumes module, the user is asked which operation to perform (Volume of Cone or Volume of Cylinder), after specifying the operation, the user is asked to input two numbers (height and radius), after that the module will respond with the result of the operation.

In the Conversion, the user is asked which operation to perform (conversion from Binary-Hexadecimal(B-TO-H) or Hexadecimal-Binary(H-TO-B)), after specifying the operation, the user is asked to input one number, after the user put the number the conversion will happen and will show you the number the user put and will show the conversion of that number